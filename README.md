- FLEX and BISON example

Example of word count using flex and bison tools.

-- to compile:

1) Create build folder
2) cd build/
3) cmake ..
4) make

-- to run the example:

1) compile
2) cd bin/
3) my_wc ../examples/example1.txt

