#ifndef YY_DECL
#define YY_DECL_IS_OURS 1
/* %if-c-only Standard (non-C++) definition */
/* %endif */
/* %if-c++-only C++ definition */
#define YY_DECL int yyFlexLexer::yylex()
/* %endif */
#endif /* !YY_DECL */